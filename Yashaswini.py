def take_file_input(file_name):
    file = open(file_name)
    return file.readlines()

def border(list_of_rows: list[str]) -> list[list]:
    return ["B" * len(list_of_rows[0])] + ["B" + row[:-1] + "B" for row in list_of_rows] + ["B" * len(list_of_rows[0])] 

def handle_rows(list_of_rows: str) -> list:
    new_list_of_rows = []
    numbering = 1
    for row_number, (prev_row, curr_row, future_row) in enumerate(zip(list_of_rows, list_of_rows[1:], list_of_rows[2:])):
        for row_column, (prev, curr, future) in enumerate(zip(curr_row, curr_row[1:], curr_row[2:]), start = 1):
            if prev + curr + future == "BWW" or prev_row[row_column] + curr + future_row[row_column] == "BWW":
                new_list_of_rows.append((row_number, row_column - 1, numbering))
                numbering += 1
    return new_list_of_rows

print(handle_rows(border(take_file_input("crosswords.txt"))))
