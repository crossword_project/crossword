def separate_into_digits(n: int) -> list[int]:
    return [digit for digit in str(n)]


def is_valid(reading: list[int]) -> bool:
    return len(set(reading)) == len(reading) and sorted(reading) == reading


def next_number(n: int) -> list[int]:
    if is_valid(separate_into_digits(n + 1)):
        return n + 1
    return next_number(n + 1)

def previous_number(n: int) -> list[int]:
    if is_valid(separate_into_digits(n + 1)):
        return n + 1
    return next_number(n + 1)

def distance(reading_1: int, reading_2: int) -> int:
    return len(list(filter(is_valid, map(separate_into_digits, range(min(reading_1, reading_2), max(reading_1,reading_2))))))

def kth_next_number(k: int, n: int) -> int:
    if k == 0:
        return n
    return kth_next_number( k - 1, next_number(n))

def kth_next_number(k: int, n: int) -> int:
    if k == 0:
        return n
    return kth_next_number( k - 1, previous_number(n))



print(next_number(789))

