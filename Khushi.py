import Yashaswini

def file_input( inp_file : str ) -> list[str] :
    return open(inp_file).split(",")


def number_first_col( first_col : str , first_row : str) -> tuple[list , int]:
    is_even =lambda num : num % 2 == 0
    numbered_1col = Yashaswini.handle_first_row(first_row)
    numbered_col = []
    numbered_col.append(Yashaswini.handle_first_row(first_row)[0][0])
    label = numbered_1col[1]
    col = 0
    row = 1
    
    for ind , ch in enumerate(first_col[1:],1):
        if(is_even(ind)) :
            if ch == "W":
                numbered_col.append((str(col), str(ind), str(label)))
                label += 1
            else :
                numbered_col.append("B")
                
        elif(first_col[ind - 1] == "B"):
            numbered_col.append((str(col), str(ind), str(label)))
            label += 1
        
        else :
            numbered_col.append(ch)
    
    return (convert_to_tuple(numbered_col), label)


def convert_to_tuple( labeled_matrix : list ) -> list[tuple] :
    return [i for i in labeled_matrix if len(i) == 3]
    

s = "W"*10 + "B" + "W"*4

print(number_first_col(s,"WWWWWWWWBWWWWWW"))
        

                
                
                
                
             
    
    
    
    
    
       

 
    
    